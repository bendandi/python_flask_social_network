import random
from random import randrange, sample
from datetime import timedelta, date
from app import db
from app.models import User


def empty_db():
	for user in User.query:
		db.session.delete(user)
	db.session.commit()

def populate_db_with_test_users():

	names = [ "Rico","Cindy","Tennie","Zita","Geralyn","Jesus",
				"Edward","Serina","Adrienne","Kandice","Lynda",
				"Marylee","Roselle","Hellen","Miguelina","Brenton",
				"Tequila","Danette","Dian","Larue","Paulita","Saul",
				"Shakira","Jeniffer","Arnoldo","Shonna","Patrick",
				"Lashaunda","Serita","Gala","Anitra","Jerilyn","Nicola",
				"Dion","Merilyn","Jule","Torrie","Carrol","Lakita","Drusilla",
				"Lenna","Lucienne","Oda","Dusti","Emil","Yoko","Marylin",
				"Wendi","Sil","Peter","Yolando","Shantel","Silas",
				"Albina","Kristel","Wava","Signe","Gaynelle","Cristine",
				"Bert","Silva","Del","Marjorie","Kesha","Rae","Harris",
				"Catrina","Tabitha","Daniella","Leopoldo","Christie",
				"Mohammed","Max","Enedina","Marguerite","Rafael","Ashlea",
				"Nydia","Shawn","Jospeh","Flora","Dann","Leonia","Alaina",
				"Myung","Wendy","Cherilyn","Shayna","Lakiesha","Luana",
				"Lucile","Leonarda","Marcos","Thurman","Octavia","Larhonda",
				"Marlin","September"]

	test_users = []

	for n in names:
		age = random.randint(1, 100)
		email = '%s@mail.com'%(n)
		test_users.append(User(password=n,
								name=n,
								age=age, 
								email=email, 
								about_me=None))

		db.session.add_all(test_users)
		db.session.commit()

	for user in test_users:
		friends = random.sample(test_users, random.randint(0, len(test_users)))
		for friend in enumerate(friends):
			user.friend(friend)
			if index == 4:
				user.set_bff(friend)

    	db.session.add_all(test_usersusers)
    	db.session.commit()

def generate_random_birthdate():
    earliast_possible_date = date(1915, 01, 01)
    latest_possible_date = date(2011, 01, 01)

    delta = latest_possible_date - earliast_possible_date
    interval = delta.total_seconds()
    random_point_in_interval = randrange(interval)

    return earliast_possible_date + timedelta(seconds=random_point_in_interval)