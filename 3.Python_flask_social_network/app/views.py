from flask import render_template, flash, redirect, session, url_for, request, g
from flask.ext.login import login_user, logout_user, current_user, login_required
from datetime import datetime, date
from flask.ext.paginate import Pagination
from app import app, db, login_manager
from .forms import RegisterForm, LoginForm, EditForm
from .models import User, friends
from config import USERS_PER_PAGE



@app.before_request
def before_request():
    g.user = current_user

@app.route('/')
@app.route('/index/<int:page>')
@app.route('/index/<sort>/<int:page>')
@login_required
def index(sort=None, page=1):
    if sort:
        if sort == 'alphabetical':
            Users = User.query.filter(User.name != g.user.name).\
                order_by(User.name)

        elif sort == 'friendCount':
            User_alias = db.aliased(User)
            subquery = db.session.query(User.id, db.func.count(friends.c.friendA).\
                        label('friend_count')).\
                        outerjoin(User_alias, User.friends).\
                        group_by(User.id).subquery()

            Users = User.query.filter(User.name != g.user.name).\
                        join(subquery, subquery.c.id == User.id).\
                        order_by(subquery.c.friend_count.desc()) 
        elif sort == 'bff':
            User_alias = db.aliased(User)
            Users = User.query.with_entities(User).outerjoin(
                User_alias,
                User.bff == User_alias.id
        ).order_by(User_alias.name.asc())

    else:
        Users = User.query.filter(User.name != g.user.name)

    users = Users.paginate(page, USERS_PER_PAGE)
    
    return render_template('index.html', users=users, sort=sort)


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    error = None
    if request.method == 'POST':
        if form.validate_on_submit():
            user = User.query.filter_by(name=form.username.data).first()

            if user != None and user.check_password(form.password.data):
                login_user(user)
                flash('Welcome {}'.format(form.username.data))
                return redirect(url_for('index'))
            else:
                error = "Invalid credentials, Please try again"
        else:
            error = "Invalid credentials, Please try again"
    return render_template('login.html', form=form, error=error)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        #age from birthdate
        today = date.today()
        cakeDay = form.age.data
        age = today.year - cakeDay.year - ((today.month, today.day) < (cakeDay.month, cakeDay.day))
        password = form.password.data
        user = User(name=form.name.data,
                        age=age, 
                        email=form.email.data, 
                        password=password, 
                        about_me=None)

        db.session.add(user)
        db.session.commit()
        login_user(user)
        return redirect(url_for('index'))
    return render_template('register.html', form=form)

@app.route('/edit', methods=['GET', 'POST'])
@login_required
def edit():
    form = EditForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            #age from birthdate
            if form.age.data:
                today = date.today()
                cakeDay = form.age.data
                g.user.age = today.year - cakeDay.year - ((today.month, today.day) < (cakeDay.month, cakeDay.day))
            if form.about_me.data:
                g.user.about_me = form.about_me.data
            if form.email.data:
                g.user.email = form.email.data
            db.session.add(g.user)
            db.session.commit()
            flash('Your changes have been saved')
            return redirect(url_for('profile', username=g.user.name))
    return render_template('edit.html', form=form)

@app.route('/delete')
@login_required
def delete():
    user = User.query.filter_by(name=g.user.name).first()
    db.session.delete(user)
    db.session.commit()
    flash('Account deleted.')
    return redirect(url_for('login'))

@app.route('/<username>')
@login_required
def profile(username):
    user = User.query.filter_by(name=username).first()
    if user == None:
        return redirect(url_for('index'))
    bff = User.query.filter_by(id=user.bff).first()
    return render_template('profile.html', user=user, bff=bff)

@app.route('/friend/<username>')
@login_required
def add_friend(username):
    user = User.query.filter_by(name=username).first()
    if user is None:
        return redirect(url_for('index'))
    if user == g.user:
        return redirect(url_for('profile', username=username))
    u = g.user.friend(user)
    if u is None:
        return redirect(url_for('profile', username=username))
    db.session.add(u)
    db.session.commit()
    flash('{} added to your friends!'.format(user.name))
    return redirect(url_for('profile', username=username))

@app.route('/unfriend/<username>')
@login_required
def unfriend(username):
    user = User.query.filter_by(name=username).first()
    if user is None:
        return redirect(url_for('index'))
    if user == g.user:
        return redirect(url_for('profile', username=username))
    u = g.user.unfriend(user)
    if u is None:
        return redirect(url_for('profile', username=username))
    db.session.add(u)
    db.session.commit()
    flash(username +' removed from your friend list!')
    return redirect(url_for('profile', username=username))

@app.route('/bff/<username>')
@login_required
def make_bff(username):
    bff = User.query.filter_by(name=username).first()
    if bff is None:
        return redirect(url_for('index'))
    if bff == g.user:
        return redirect(url_for('profile', username=username))
    u = g.user.set_bff(bff)
    if u is None:
        return redirect(url_for('profile', username=username))
    db.session.add(u)
    db.session.commit()
    flash(username +' is now your very best friend')
    return redirect(url_for('profile', username=username))








