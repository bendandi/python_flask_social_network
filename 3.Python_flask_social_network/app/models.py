from hashlib import md5
from app import db
from app import app
from werkzeug.security import generate_password_hash, check_password_hash


friends = db.Table(
    'friends',
    db.Column('friendA', db.Integer, db.ForeignKey('user.id', ondelete="CASCADE")),
    db.Column('friendB', db.Integer, db.ForeignKey('user.id', ondelete="CASCADE"))
)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    pw_hash = db.Column(db.String)
    name = db.Column(db.String(64), index=True, unique=True)
    age = db.Column(db.Integer)
    email = db.Column(db.String(120))
    about_me = db.Column(db.String(140))
    bff = db.Column(db.Integer, db.ForeignKey('user.id',  ondelete="SET NULL"))
    best_friend_of = db.relationship(
        'User',
        backref=db.backref('best_friend', remote_side=[id]),
        lazy='dynamic',
        post_update=True
    )    
    
    friended = db.relationship('User',
                               secondary=friends,
                               primaryjoin=(friends.c.friendA == id),
                               secondaryjoin=(friends.c.friendB == id),
                               backref=db.backref('friends', lazy='dynamic'),
                               lazy='dynamic')

    def __init__(self, password, name, age, email, about_me):
        self.set_password(password)
        self.name = name
        self.age = age
        self.email = email
        self.about_me = about_me

    def set_password(self, password):
        self.pw_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.pw_hash, password)

    def avatar(self, size):
        return 'http://www.gravatar.com/avatar/%s?d=mm&s=%d' % \
            (md5(self.email.encode('utf-8')).hexdigest(), size)

    def friend(self, user):
        if not self.is_friends(user):
            self.friended.append(user)
            user.friended.append(self)
            return self

    def unfriend(self, user):
        if self.is_friends(user):
            self.friended.remove(user)
            user.friended.remove(self)
            if self.bff == user.id:
                self.bff = None
            return self

    def set_bff(self, user):
        if not self.is_friends(user):
            self.friended.append(user)
            user.friended.append(self)
            
        self.bff = user.id
        return self

    def is_friends(self, user):
        return self.friended.filter(
            friends.c.friendB == user.id).count() > 0

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User %r>' % (self.name)








