from flask.ext.wtf import Form
from wtforms import StringField, BooleanField, TextAreaField, DateField, PasswordField
from wtforms.validators import DataRequired, Length
from .models import User


class LoginForm(Form):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
 
class RegisterForm(Form):
	name = StringField('name', validators=[DataRequired()])
	age = DateField('cakeDay', validators=[DataRequired()])
	email = StringField('email', validators=[DataRequired()])
	password = PasswordField('password', validators=[DataRequired()])

	def validate(self):
		if not Form.validate(self):
			return False
		user = User.query.filter_by(name=self.name.data).first()
		if user is not None:
			self.name.errors.append('This username is already in use. Please choose another one.')
			return False
		return True

class EditForm(Form):
	age = DateField('cakeDay')
	email = StringField('email')
	about_me = TextAreaField('about_me')

	def validate(self):
		if not Form.validate(self):
			return False
		return True

