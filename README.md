Social network. Flask test run

Features

*  Register user
*  Authentication
*  View user profile
*  Edit user
*  Remove user
*  Friend user
*  Unfriend user
*  Add best friend
*  Each user can only have one best friend
*  List users
*  Show user names, name of the best friend (if any) and number of friends
*  Sortable by name, name of the best friend and number of friends
*  Pagination1. *